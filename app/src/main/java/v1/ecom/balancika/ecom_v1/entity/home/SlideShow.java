
package v1.ecom.balancika.ecom_v1.entity.home;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class SlideShow {

    @SerializedName("imgShow")
    private String mImgShow;
    @SerializedName("slideUrl")
    private String mSlideUrl;

    public String getmImgShow() {
        return mImgShow;
    }

    public void setmImgShow(String mImgShow) {
        this.mImgShow = mImgShow;
    }

    public String getmSlideUrl() {
        return mSlideUrl;
    }

    public void setmSlideUrl(String mSlideUrl) {
        this.mSlideUrl = mSlideUrl;
    }

    public String getImgShow() {
        return mImgShow;
    }

    public void setImgShow(String imgShow) {
        mImgShow = imgShow;
    }

    public String getSlideUrl() {
        return mSlideUrl;
    }

    public void setSlideUrl(String slideUrl) {
        mSlideUrl = slideUrl;
    }

    @Override
    public String toString() {
        return "SlideShow{" +
                "mImgShow='" + mImgShow + '\'' +
                ", mSlideUrl='" + mSlideUrl + '\'' +
                '}';
    }
}
