package v1.ecom.balancika.ecom_v1.repository;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import v1.ecom.balancika.ecom_v1.entity.home.HomeEntity;
import v1.ecom.balancika.ecom_v1.entity.home.hot.HotProduct;


/**
 * Created by admin on 6/7/18.
 */

public interface Home {
    @GET("api/mobile/home/list/{popularLimit}")
    Call<HomeEntity> getHome(@Path("popularLimit") int popularLimit);

    @GET("api/mobile/home/list/{hotNum}/{hotPage}")
    Call<HotProduct> getHotProdut(@Path("hotNum") int hotNum,@Path("hotPage") int hotPage);
}
