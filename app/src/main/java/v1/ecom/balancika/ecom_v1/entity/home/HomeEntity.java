
package v1.ecom.balancika.ecom_v1.entity.home;

import java.util.List;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class HomeEntity {

    @SerializedName("itemsPopular")
    private List<ItemsPopular> mItemsPopular;
    @SerializedName("msg")
    private String mMsg;
    @SerializedName("shopItems")
    private List<ShopItem> mShopItems;
    @SerializedName("shopProfile")
    private ShopProfile mShopProfile;
    @SerializedName("slideShow")
    private List<SlideShow> mSlideShow;
    @SerializedName("status")
    private Long mStatus;

    public List<ItemsPopular> getItemsPopular() {
        return mItemsPopular;
    }

    public void setItemsPopular(List<ItemsPopular> itemsPopular) {
        mItemsPopular = itemsPopular;
    }

    public String getMsg() {
        return mMsg;
    }

    public void setMsg(String msg) {
        mMsg = msg;
    }

    public List<ShopItem> getShopItems() {
        return mShopItems;
    }

    public void setShopItems(List<ShopItem> shopItems) {
        mShopItems = shopItems;
    }

    public ShopProfile getShopProfile() {
        return mShopProfile;
    }

    public void setShopProfile(ShopProfile shopProfile) {
        mShopProfile = shopProfile;
    }

    public List<SlideShow> getSlideShow() {
        return mSlideShow;
    }

    public void setSlideShow(List<SlideShow> slideShow) {
        mSlideShow = slideShow;
    }

    public Long getStatus() {
        return mStatus;
    }

    public void setStatus(Long status) {
        mStatus = status;
    }

    @Override
    public String toString() {
        return "HomeEntity{" +
                "mItemsPopular=" + mItemsPopular +
                ", mMsg='" + mMsg + '\'' +
                ", mShopItems=" + mShopItems +
                ", mShopProfile=" + mShopProfile +
                ", mSlideShow=" + mSlideShow +
                ", mStatus=" + mStatus +
                '}';
    }
}
