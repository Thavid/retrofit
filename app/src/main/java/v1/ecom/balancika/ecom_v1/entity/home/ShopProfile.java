
package v1.ecom.balancika.ecom_v1.entity.home;

import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class ShopProfile {

    @SerializedName("imgUrl")
    private String mImgUrl;
    @SerializedName("sId")
    private Long mSId;
    @SerializedName("sName")
    private String mSName;

    public String getImgUrl() {
        return mImgUrl;
    }

    public void setImgUrl(String imgUrl) {
        mImgUrl = imgUrl;
    }

    public Long getSId() {
        return mSId;
    }

    public void setSId(Long sId) {
        mSId = sId;
    }

    public String getSName() {
        return mSName;
    }

    public void setSName(String sName) {
        mSName = sName;
    }

    @Override
    public String toString() {
        return "ShopProfile{" +
                "mImgUrl='" + mImgUrl + '\'' +
                ", mSId=" + mSId +
                ", mSName='" + mSName + '\'' +
                '}';
    }
}

