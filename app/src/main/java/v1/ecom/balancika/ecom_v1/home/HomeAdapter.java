package v1.ecom.balancika.ecom_v1.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import v1.ecom.balancika.ecom_v1.R;
import v1.ecom.balancika.ecom_v1.entity.home.ItemsPopular;

/**
 * Created by admin on 6/10/18.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {
    List<ItemsPopular> listPopular;
    Context context;

    public HomeAdapter(List<ItemsPopular> listPopular, Context context) {
        this.listPopular = listPopular;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_popular_item,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ItemsPopular itemsPopular = listPopular.get(position);
        if (!itemsPopular.getImgPath().isEmpty()){
            Picasso.with(context).load(itemsPopular.getImgPath()).into(holder.imageView);
        }
        holder.txtProductName.setText(itemsPopular.getItemName());
        holder.txtItemPrice.setText(itemsPopular.getPrice()+"");
    }

    @Override
    public int getItemCount() {
        return listPopular.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public ImageView imageView;
        public TextView txtProductName , txtItemPrice;
        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_popular);
            txtProductName = itemView.findViewById(R.id.txtPopularName);
            txtItemPrice = itemView.findViewById(R.id.txtPopularPrice);
        }
    }
}

