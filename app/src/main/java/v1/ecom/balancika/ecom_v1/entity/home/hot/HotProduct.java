
package v1.ecom.balancika.ecom_v1.entity.home.hot;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class HotProduct {

    @SerializedName("hotItem")
    private List<HotItem> mHotItem;
    @SerializedName("msg")
    private String mMsg;
    @SerializedName("status")
    private Long mStatus;
    @SerializedName("totalPage")
    private Long mTotalPage;

    public List<HotItem> getHotItem() {
        return mHotItem;
    }

    public void setHotItem(List<HotItem> hotItem) {
        mHotItem = hotItem;
    }

    public String getMsg() {
        return mMsg;
    }

    public void setMsg(String msg) {
        mMsg = msg;
    }

    public Long getStatus() {
        return mStatus;
    }

    public void setStatus(Long status) {
        mStatus = status;
    }

    public Long getTotalPage() {
        return mTotalPage;
    }

    public void setTotalPage(Long totalPage) {
        mTotalPage = totalPage;
    }

    @Override
    public String toString() {
        return "HotProduct{" +
                "mHotItem=" + mHotItem +
                ", mMsg='" + mMsg + '\'' +
                ", mStatus=" + mStatus +
                ", mTotalPage=" + mTotalPage +
                '}';
    }
}
