package v1.ecom.balancika.ecom_v1.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import v1.ecom.balancika.ecom_v1.R;
import v1.ecom.balancika.ecom_v1.entity.home.hot.HotItem;

/**
 * Created by admin on 6/11/18.
 */

public class HotAdapter extends RecyclerView.Adapter<HotAdapter.MyviewHoder> {
    List<HotItem> listHotItem;
    Context context;

    public HotAdapter(List<HotItem> listHotItem, Context context) {
        this.listHotItem = listHotItem;
        this.context = context;
    }

    @Override
    public MyviewHoder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_hot_item,parent,false);
        return new MyviewHoder(view);
    }

    @Override
    public void onBindViewHolder(MyviewHoder holder, int position) {
        HotItem hotItem = listHotItem.get(position);
        if(!hotItem.getImgPath().isEmpty()){
            Picasso.with(context).load(hotItem.getImgPath()).into(holder.hotImage);
        }
        holder.txtHotName.setText(hotItem.getItemName());
        holder.txtHotPrice.setText(hotItem.getPrice()+"");
    }

    @Override
    public int getItemCount() {
        return listHotItem.size();
    }

    class  MyviewHoder extends RecyclerView.ViewHolder{
        ImageView hotImage;
        TextView txtHotName,txtHotPrice;
        public MyviewHoder(View itemView) {
            super(itemView);
            hotImage = itemView.findViewById(R.id.hotImg);
            txtHotName = itemView.findViewById(R.id.hotName);
            txtHotPrice = itemView.findViewById(R.id.hotPrice);
        }
    }
}
