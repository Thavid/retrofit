
package v1.ecom.balancika.ecom_v1.entity.home;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class ShopItem {

    @SerializedName("cId")
    private Long mCId;
    @SerializedName("feature")
    private String mFeature;
    @SerializedName("imgPath")
    private String mImgPath;
    @SerializedName("itemCode")
    private String mItemCode;
    @SerializedName("itemName")
    private String mItemName;
    @SerializedName("price")
    private Long mPrice;
    @SerializedName("sId")
    private Long mSId;
    @SerializedName("scId")
    private Long mScId;
    @SerializedName("sysCode")
    private Long mSysCode;

    public Long getCId() {
        return mCId;
    }

    public void setCId(Long cId) {
        mCId = cId;
    }

    public String getFeature() {
        return mFeature;
    }

    public void setFeature(String feature) {
        mFeature = feature;
    }

    public String getImgPath() {
        return mImgPath;
    }

    public void setImgPath(String imgPath) {
        mImgPath = imgPath;
    }

    public String getItemCode() {
        return mItemCode;
    }

    public void setItemCode(String itemCode) {
        mItemCode = itemCode;
    }

    public String getItemName() {
        return mItemName;
    }

    public void setItemName(String itemName) {
        mItemName = itemName;
    }

    public Long getPrice() {
        return mPrice;
    }

    public void setPrice(Long price) {
        mPrice = price;
    }

    public Long getSId() {
        return mSId;
    }

    public void setSId(Long sId) {
        mSId = sId;
    }

    public Long getScId() {
        return mScId;
    }

    public void setScId(Long scId) {
        mScId = scId;
    }

    public Long getSysCode() {
        return mSysCode;
    }

    public void setSysCode(Long sysCode) {
        mSysCode = sysCode;
    }

    @Override
    public String toString() {
        return "ShopItem{" +
                "mCId=" + mCId +
                ", mFeature='" + mFeature + '\'' +
                ", mImgPath='" + mImgPath + '\'' +
                ", mItemCode='" + mItemCode + '\'' +
                ", mItemName='" + mItemName + '\'' +
                ", mPrice=" + mPrice +
                ", mSId=" + mSId +
                ", mScId=" + mScId +
                ", mSysCode=" + mSysCode +
                '}';
    }
}
