package v1.ecom.balancika.ecom_v1.home;

import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.ViewTreeObserver;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.io.Console;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import v1.ecom.balancika.ecom_v1.R;
import v1.ecom.balancika.ecom_v1.entity.home.HomeEntity;
import v1.ecom.balancika.ecom_v1.entity.home.ItemsPopular;
import v1.ecom.balancika.ecom_v1.entity.home.hot.HotItem;
import v1.ecom.balancika.ecom_v1.entity.home.hot.HotProduct;
import v1.ecom.balancika.ecom_v1.repository.Home;
import v1.ecom.balancika.ecom_v1.service.Constance;
import v1.ecom.balancika.ecom_v1.service.ServiceGenerator;

public class HomePag extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    private SliderLayout mDemoSlider;
    private ImageView iconDrawer;
    private ActionBarDrawerToggle toggle;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private List<String> dataList;
    HashMap<String,String> file_maps;

    HomeAdapter homeAdapter;
    List<ItemsPopular> listPopular;
    RecyclerView popularRecyclerView;

    NestedScrollView mScrollView;
    HotAdapter hotAdapter;
    List<HotItem> listHot;
    RecyclerView hotRecyclerview;
    int page = 1;
    Home home;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        initializeControll();
        navigationDrawer();
       // hotRecyclerview.setNestedScrollingEnabled(false);

        home = ServiceGenerator.createService(Home.class, Constance.authToken);
        Call<HomeEntity> call = home.getHome(10);
        call.enqueue(new Callback<HomeEntity>() {
            @Override
            public void onResponse(Call<HomeEntity> call, Response<HomeEntity> response) {
                if (response.isSuccessful()) {
                    Log.e("Size",response.body().toString());
                    for(int i=0;i<response.body().getSlideShow().size();i++){
                        dataList.add(response.body().getSlideShow().get(i).getmImgShow());

                    }
                    slideShow(dataList);
                    //Log.e("Size->",response.body().getItemsPopular().size()+"");
                    getPopular(response.body(),response.body().getItemsPopular().size());

                } else {
                    Log.e("Error->",response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<HomeEntity>call, Throwable t) {
                Log.e("Error->",t.getMessage());
            }
        });

        Call<HotProduct> callHot = home.getHotProdut(20,page);
        callHot.enqueue(new Callback<HotProduct>() {
            @Override
            public void onResponse(Call<HotProduct> call, Response<HotProduct> response) {
                if(response.isSuccessful()){
                    getHot(response.body(),response.body().getHotItem().size());
                    Log.e("Data->",response.body().toString());
                }else {
                    Log.e("Message->",response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<HotProduct> call, Throwable t) {
                    Log.e("Fail->",t.getMessage());
            }
        });

    }

    /*End OnCreate.....*/
    public void initializeControll(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        iconDrawer = findViewById(R.id.iconDrawer);
        dataList = new ArrayList();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public void navigationDrawer(){
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        iconDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);
            }
        });
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void getPopular(HomeEntity homeEntity,int itemCount){
        listPopular = new ArrayList<>();
        popularRecyclerView = findViewById(R.id.popularRecyclerView);
        homeAdapter = new HomeAdapter(listPopular,this);
        LinearLayoutManager layoutManager = new  LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        popularRecyclerView.setLayoutManager(layoutManager);
        for(int i =0 ; i<itemCount;i++){
            listPopular.add(new ItemsPopular(homeEntity.getItemsPopular().get(i).getImgPath(),
                                             homeEntity.getItemsPopular().get(i).getItemName(),
                                             homeEntity.getItemsPopular().get(i).getPrice()));
        }
        popularRecyclerView.setAdapter(homeAdapter);
    }
    public void slideShow(List<String> imgUrl){
        mDemoSlider = (SliderLayout)findViewById(R.id.slider);
        file_maps = new HashMap<String, String>();
        for(int i=0;i<imgUrl.size();i++) {
            file_maps.put("Hannibal", imgUrl.get(i));
            Log.e("Slide->", imgUrl.get(i));


            for (String name : file_maps.keySet()) {
                TextSliderView textSliderView = new TextSliderView(this);
                // initialize a SliderLayout
                textSliderView

                        .image(file_maps.get(name))
                        .setScaleType(BaseSliderView.ScaleType.Fit)
                        .setOnSliderClickListener(this);

                //add your extra information
                textSliderView.bundle(new Bundle());
                textSliderView.getBundle()
                        .putString("extra", name);

                mDemoSlider.addSlider(textSliderView);
            }
            mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
            mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            mDemoSlider.setCustomAnimation(new DescriptionAnimation());
            mDemoSlider.setDuration(4000);
            mDemoSlider.addOnPageChangeListener(this);
        }
    }

    public void getHot(HotProduct hotProduct,int itemCount){
        listHot = new ArrayList<>();
        hotRecyclerview = findViewById(R.id.hotRecyclerview);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this,2);
        hotAdapter = new HotAdapter(listHot,this);
        hotRecyclerview.setLayoutManager(layoutManager);
        for(int i=0;i<itemCount;i++){
            listHot.add(new HotItem(hotProduct.getHotItem().get(i).getImgPath()
                                    ,hotProduct.getHotItem().get(i).getItemName()
                                    ,hotProduct.getHotItem().get(i).getPrice()));
        }
        hotRecyclerview.setAdapter(hotAdapter);
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
